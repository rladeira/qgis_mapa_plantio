# -*- coding: latin-1 -*-
from qgis.core import *
from PyQt4.QtGui import QFileDialog
import os.path

class SaveProjectException(Exception):
    pass
class SaveLayerException(Exception):
    pass

class ProjectManager:

    def __init__(self):
        self.base_path = ""
        self.path = ""
        
    def save_project(self):
        self.select_output_directory()

        layers = QgsMapLayerRegistry.instance().mapLayers().values()
        
        for layer in layers:
            try:
                self.save_layer(layer) 

            except SaveProjectException as e:
                raise SaveProjectException("Error saving project: " + e.message_text())
        
        self.save_project_file()

    def select_output_directory(self):
        if QgsProject.instance().fileName() == "":
            self.path = QFileDialog.getSaveFileName(None, u'Save project', os.path.expanduser('~'), "Qgis Files(*.qgs)")
        else:
            self.path = QgsProject.instance().fileName()
        self.base_path = os.path.dirname(self.path)

    def save_project_file(self):
        QgsProject.instance().setFileName(self.path)
        QgsProject.instance().write()
        
    def save_layer(self, layer):
        if layer.name() == "Google Satellite":
            return
        if layer.providerType() == "ogr":
            return
        filename = os.path.join(self.base_path, layer.name() + ".geojson")
        writer =  QgsVectorFileWriter.writeAsVectorFormat(layer, filename, u"latin1", None, "GeoJSON")
        if writer != QgsVectorFileWriter.NoError:
            print "Erro: " + writer
            raise SaveLayerException("Error saving layer %s" % (layer.name()) )
        new_layer = QgsVectorLayer(filename, layer.name(), "ogr")
        renderer = layer.rendererV2().clone()
        
        QgsMapLayerRegistry.instance().removeMapLayer(layer.id())
        QgsMapLayerRegistry.instance().addMapLayer(new_layer)
        
        new_layer.setRendererV2(renderer)
