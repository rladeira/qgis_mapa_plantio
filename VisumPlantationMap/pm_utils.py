# -*- coding: latin-1 -*-
from qgis.core import QgsMapLayerRegistry, QgsMapLayer, QGis, QgsFeatureRequest
import re

def populate_layerBox(combobox):
    """ Add all vectorial layers to combo box """

    combobox.clear()
    layers = QgsMapLayerRegistry.instance().mapLayers().values()
    for layer in layers:
        if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() == QGis.Point:
            combobox.addItem(layer.name(), layer)
            
def build_request(field, expression, operator, length):
    query = ""
    for i in range(0, length):
            if i == 0:
                query += '"' + field + '"'
            else:
                query += '"' + field + '_' + str(i) + '"' 
                
            query += " " + expression
            
            if i != length - 1:
                query += " " + operator + " "
    
    return QgsFeatureRequest().setFilterExpression(query)

#retirado de: http://stackoverflow.com/questions/4836710/does-python-have-a-built-in-function-for-string-natural-sort
def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)
    