# -*- coding: latin-1 -*-
from qgis.core import *
from PyQt4 import *
from PyQt4.QtGui import *
from PyQt4.Qt import *

class SpeedMapManager:
    
    SYMBOL_SIZE = 4

    def __init__(self, iface=None):
        self.iface = iface

    def create_map(self, layer, new_layer_name, number_of_classes):
        glayer = self.duplicate_layer(layer, new_layer_name)
        self.apply_graduation(glayer, number_of_classes)
    
    def duplicate_layer(self, layer, new_layer_name):
        new_layer = QgsVectorLayer(layer.source(), new_layer_name, layer.providerType())
        QgsMapLayerRegistry.instance().addMapLayer(new_layer)
        return new_layer
    
    def apply_graduation(self,layer,number_of_classes):
        fieldName = u'VELOCIDADE M�DIA [Km/h]'

        fieldIndex = layer.pendingFields().indexFromName(fieldName)
        
        provider = layer.dataProvider()
        
        minimum = provider.minimumValue( fieldIndex )
        maximum = provider.maximumValue( fieldIndex )
        ranges = []
    

        for i in range( number_of_classes ):
            symbol = QgsMarkerSymbolV2.defaultSymbol(layer.geometryType())
            symbol.setSize(self.SYMBOL_SIZE)

            lower = ('%.*f' % (2, minimum + ( maximum - minimum ) / number_of_classes * i ) )
            upper = ('%.*f' % (2, minimum + ( maximum - minimum ) / number_of_classes * ( i + 1 ) ) )
                    
            label = lower + " - " + upper
        
            sy = QgsRendererRangeV2(float(lower), float(upper), symbol, label )
 
        
            ranges.append(sy)
   
        renderer = QgsGraduatedSymbolRendererV2( fieldName, ranges )
        renderer.setMode( QgsGraduatedSymbolRendererV2.EqualInterval ) 
        renderer.updateColorRamp(QgsStyleV2.defaultStyle().colorRamp(u'Spectral'))
        
        layer.setRendererV2( renderer )
        
        