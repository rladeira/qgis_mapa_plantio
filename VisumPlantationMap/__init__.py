# -*- coding: latin-1 -*-
"""
/***************************************************************************
 VisumMapOfPlanting
                                 A QGIS plugin
 Plugin para gerar de forma f�cil mapas de planta��es agr�colas com dados obtidos do sistema Visum
                             -------------------
        begin                : 2016-07-11
        copyright            : (C) 2016 by J. Assy Apollo Agr�cola
        email                : txai.garcia@assy.com.br
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load VisumMapOfPlanting class from file VisumMapOfPlanting.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from visum_map_of_planting import VisumMapOfPlanting
    return VisumMapOfPlanting(iface)
