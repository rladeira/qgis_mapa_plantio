# -*- coding: latin-1 -*-

import os

from PyQt4 import uic
from PyQt4.QtGui import QFileDialog, QDialog, QMessageBox
import ntpath
from file_import import FileImportManager

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'file_import_dialog.ui'))


class FileImportDialog(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(FileImportDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.iface = None
        self.setupUi(self)
        self.txtFilePath.clear()
        self.txtLayerName.clear()
        self.btSearch.clicked.connect(self.select_input_file)
        self.btOk.clicked.connect(self.proccess_task)
        self.btCancel.clicked.connect(lambda: self.reject())

    def initGui(self, iface):
        self.iface = iface
        self.txtFilePath.clear()
        self.txtLayerName.clear()
    
    def select_input_file(self):

        filename = QFileDialog.getOpenFileName(self, self.tr('Select the file'), "", "CSV File (*.csv)")
        self.txtFilePath.setText(filename)
        self.txtLayerName.setText(ntpath.splitext(ntpath.basename(filename))[0])

    def proccess_task(self):
        if not os.path.exists(self.txtFilePath.text()):
            QMessageBox.critical(self, self.tr(u'Error'), self.tr(u'Invalid or inexistent file'))
            return
        
        if self.txtLayerName.text().isspace() or not self.txtLayerName.text():
            QMessageBox.critical(self, self.tr(u'Error'), self.tr(u'Layer name is mandatory'))
            return
                                                
        file_mng = FileImportManager(self.iface)
        file_mng.create_layer_from_file(self.txtFilePath.text(), self.txtLayerName.text())
        self.accept()