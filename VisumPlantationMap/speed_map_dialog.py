# -*- coding: latin-1 -*-

import os

from PyQt4 import QtGui, uic
from PyQt4.QtGui import QMessageBox
from pm_utils import populate_layerBox
from speed_map import SpeedMapManager

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'speed_map_dialog.ui'))


class SpeedMapDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(SpeedMapDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        self.iface = None
        self.setupUi(self)
        self.txtLayerName.clear()
        self.txtNumSpeed.setText('5')
        self.btOk.clicked.connect(self.proccess_task)
        self.btCancel.clicked.connect(lambda: self.reject())
        self.cmbVLayer.currentIndexChanged.connect(self.update_layer_name)

    def initGui(self, iface):
        self.iface = iface
        self.txtLayerName.clear()
        self.txtNumSpeed.setText('5')
        populate_layerBox(self.cmbVLayer)

    def update_layer_name(self):
        if(self.cmbVLayer.currentIndex() >= 0):
            layer_name = self.cmbVLayer.itemData(self.cmbVLayer.currentIndex()).name()
            self.txtLayerName.setText(layer_name + "_vel")

    def proccess_task(self):                
        if(self.cmbVLayer.currentIndex() < 0):
            self.accept()
            return
            
        if self.txtLayerName.text().isspace() or not self.txtLayerName.text():
            QMessageBox.critical(self, self.tr(u'Error'), self.tr(u'Layer name is mandatory'))
            return
        
        if self.txtNumSpeed.text().isspace() or not self.txtNumSpeed.text():
            QMessageBox.critical(self, self.tr(u'Error'), self.tr(u'Number of classes is mandatory'))
            return

        layer = self.cmbVLayer.itemData(self.cmbVLayer.currentIndex())
        manager = SpeedMapManager(self.iface)
        manager.create_map(layer, self.txtLayerName.text(), int(self.txtNumSpeed.text()))
        self.accept()