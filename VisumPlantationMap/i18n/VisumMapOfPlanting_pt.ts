<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../visum_plantation_map.py" line="81"/>
        <source>&amp;Mapa de plantio</source>
        <translation type="obsolete">&amp;Plantation map</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="297"/>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="71"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="61"/>
        <source>Selecione o arquivo de dados</source>
        <translation type="obsolete">Select the file</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="71"/>
        <source>Erro de importaÃ§Ã£o</source>
        <translation type="obsolete">Importation error</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="67"/>
        <source>Arquivo invÃ¡lido ou inexistente</source>
        <translation type="obsolete">Invalid file or file not found</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="71"/>
        <source>Nome da camada Ã© obrigatÃ³rio</source>
        <translation type="obsolete">Please insert layer name</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="71"/>
        <source>Nome da camada é obrigatório</source>
        <translation type="obsolete">Please insert layer name</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="67"/>
        <source>Arquivo inváido ou inexistente</source>
        <translation type="obsolete">Invalid file or file not found</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="290"/>
        <source>Projeto salvo com sucesso</source>
        <translation type="obsolete">Project succesfully saved</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="292"/>
        <source>Erro ao salvar projeto</source>
        <translation type="obsolete">Error saving project</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="80"/>
        <source>&amp;J.Assy - Mapa de plantio</source>
        <translation type="obsolete">&amp;J.Assy - Map of planting</translation>
    </message>
</context>
<context>
    <name>AlarmMapDialog</name>
    <message>
        <location filename="../alarm_map_dialog.ui" line="14"/>
        <source>Mapa de alarme</source>
        <translation type="obsolete">Alarm map</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="29"/>
        <source>Mapa do Visum</source>
        <translation type="obsolete">Visum map layer</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="51"/>
        <source>Nome da camada de rota</source>
        <translation type="obsolete">Layer name</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="73"/>
        <source>Cancelar</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="80"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.py" line="69"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.py" line="69"/>
        <source>Nome da camada é obrigatório</source>
        <translation type="obsolete">Please insert layer name</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.py" line="69"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.py" line="69"/>
        <source>Layer name is mandatory</source>
        <translation>Nome da camada é obrigatório</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="14"/>
        <source>Visum alarm map</source>
        <translation>Mapa de alarme do Visum</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="29"/>
        <source>Layer</source>
        <translation>Camada</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="51"/>
        <source>Map name</source>
        <translation>Nome do mapa</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="73"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../alarm_map_dialog.ui" line="80"/>
        <source>Cancelar</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="80"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="80"/>
        <source>Mapa do Visum</source>
        <translation type="obsolete">Visum map layer</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="80"/>
        <source>Faixas de velocidade</source>
        <translation type="obsolete">Classes</translation>
    </message>
    <message>
        <location filename="../alarm_map_dialog.ui" line="80"/>
        <source>Nome da camada</source>
        <translation type="obsolete">Layer name</translation>
    </message>
</context>
<context>
    <name>FileImportDialog</name>
    <message>
        <location filename="../file_import_dialog.ui" line="14"/>
        <source>Importar arquivo Visum</source>
        <translation type="obsolete">Import file from Visum</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="41"/>
        <source>Buscar</source>
        <translation type="obsolete">Search</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="52"/>
        <source>Digite o nome da camada</source>
        <translation type="obsolete">Insert layer name</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="73"/>
        <source>Cancelar</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="86"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="60"/>
        <source>Selecione o arquivo de dados</source>
        <translation type="obsolete">Select the file</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="70"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="66"/>
        <source>Arquivo inváido ou inexistente</source>
        <translation type="obsolete">Invalid file or file not found</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="70"/>
        <source>Nome da camada é obrigatório</source>
        <translation type="obsolete">Please insert layer name</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="38"/>
        <source>Select the file</source>
        <translation>Selecione o arquivo</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="48"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="44"/>
        <source>Invalid or inexistent file</source>
        <translation>Arquivo inválido ou inexistente</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.py" line="48"/>
        <source>Layer name is mandatory</source>
        <translation>Nome da camada é obrigatório</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="14"/>
        <source>Import file from Visum</source>
        <translation>Importar arquivo do Visum</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="41"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="52"/>
        <source>Map name</source>
        <translation>Nome do mapa</translation>
    </message>
    <message>
        <location filename="../file_import_dialog.ui" line="73"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>SpeedMapDialog</name>
    <message>
        <location filename="../speed_map_dialog.ui" line="14"/>
        <source>Mapa de velocidade</source>
        <translation type="obsolete">Speed map</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="29"/>
        <source>Cancelar</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="36"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="58"/>
        <source>Mapa do Visum</source>
        <translation type="obsolete">Visum map layer</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="77"/>
        <source>Faixas de velocidade</source>
        <translation type="obsolete">Classes</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="99"/>
        <source>Nome da camada</source>
        <translation type="obsolete">Layer name</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="75"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="71"/>
        <source>Nome da camada é obrigatório</source>
        <translation type="obsolete">Please insert layer name</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="75"/>
        <source>Número de classes é obrigatório</source>
        <translation type="obsolete">Number of classes is required</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="54"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="50"/>
        <source>Layer name is mandatory</source>
        <translation>Nome da camada é obrigatório</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.py" line="54"/>
        <source>Number of classes is mandatory</source>
        <translation>Número de classes é obrigatório</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="14"/>
        <source>Tractor speed map</source>
        <translation>Mapa de velocidade do trator</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="29"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="58"/>
        <source>Layer</source>
        <translation>Camada</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="77"/>
        <source>Number of classes</source>
        <translation>Número de classes</translation>
    </message>
    <message>
        <location filename="../speed_map_dialog.ui" line="99"/>
        <source>Map name</source>
        <translation>Nome do mapa</translation>
    </message>
</context>
<context>
    <name>TractorMapDialog</name>
    <message>
        <location filename="../tractor_map_dialog.py" line="69"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.py" line="69"/>
        <source>Nome da camada é obrigatório</source>
        <translation type="obsolete">Please insert layer name</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.py" line="48"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.py" line="48"/>
        <source>Layer name is mandatory</source>
        <translation>Nome da camada é obrigatório</translation>
    </message>
</context>
<context>
    <name>TratorMapDialog</name>
    <message>
        <location filename="../tractor_map_dialog.ui" line="14"/>
        <source>Mapa do trator</source>
        <translation type="obsolete">Tractor route map</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="29"/>
        <source>Mapa do Visum</source>
        <translation type="obsolete">Visum map layer</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="51"/>
        <source>Nome da camada de rota</source>
        <translation type="obsolete">Layer name</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="73"/>
        <source>Cancelar</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="80"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="14"/>
        <source>Tractor route map</source>
        <translation>Mapa de rota do trator</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="29"/>
        <source>Layer</source>
        <translation>Camada</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="51"/>
        <source>Map name</source>
        <translation>Nome do mapa</translation>
    </message>
    <message>
        <location filename="../tractor_map_dialog.ui" line="73"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>VisumMapOfPlanting</name>
    <message>
        <location filename="../visum_map_of_planting.py" line="226"/>
        <source>&amp;J.Assy - Mapa de plantio</source>
        <translation type="obsolete">&amp;J.Assy - Map of planting</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="179"/>
        <source>Importar arquivo do Visum</source>
        <translation type="obsolete">Import file from Visum</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="186"/>
        <source>Adicionar mapa de satelite</source>
        <translation type="obsolete">Add satellite map</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="193"/>
        <source>Criar mapa de rota do trator</source>
        <translation type="obsolete">Create tractor route map</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="200"/>
        <source>Criar mapa de velocidades do trator</source>
        <translation type="obsolete">Create tractor speed map</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="207"/>
        <source>Criar mapa de alarmes do Visum</source>
        <translation type="obsolete">Create Visum alarm map</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="302"/>
        <source>Projeto salvo com sucesso</source>
        <translation type="obsolete">Project succesfully saved</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="304"/>
        <source>Erro ao salvar projeto</source>
        <translation type="obsolete">Error saving project</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="310"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="312"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="261"/>
        <source>QuickMapServices não está instalado. Por favor instale o plugin e tente novamente</source>
        <translation type="obsolete">QuickMapServices is not installed. Please install the plugin and try again</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="271"/>
        <source>QuickMapServices não está configurado. Por favor, configure o plugin</source>
        <translation type="obsolete">QuickMapServices is not configured. Please, configure the plugin</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="227"/>
        <source>&amp;J.Assy - Map of planting</source>
        <translation>&amp;J.Assy - Mapa de plantio</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="180"/>
        <source>Import file from Visum</source>
        <translation>Importar arquivo do Visum</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="187"/>
        <source>Add satellite map</source>
        <translation>Adicionar mapa de satélite</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="194"/>
        <source>Add tractor route map</source>
        <translation>Adicionar mapa de rota do trator</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="201"/>
        <source>Add tractor speed map</source>
        <translation>Adicionar mapa de velocidades do trator</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="208"/>
        <source>Add Visum alarm map</source>
        <translation>Adicionar mapa de alarmes do Visum</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="262"/>
        <source>QuickMapServices is not installed. Please install the plugin and try again</source>
        <translation>QuickMapServices não está instalado. Por favor, instale o plugin e tente novamente</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="272"/>
        <source>QuickMapServices is not configured. Please, configure the plugin and try again</source>
        <translation>QuickMapServices não está configurado. Por favor, configure o plugin e tente novamente</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="303"/>
        <source>Project saved successfully</source>
        <translation>Projeto salvo com sucesso</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="305"/>
        <source>Error saving the project</source>
        <translation>Erro salvando o projeto</translation>
    </message>
    <message>
        <location filename="../visum_map_of_planting.py" line="313"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
</context>
<context>
    <name>VisumPlantationMap</name>
    <message>
        <location filename="../visum_plantation_map.py" line="179"/>
        <source>Importar arquivo do Visum</source>
        <translation type="obsolete">Import file from Visum</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="186"/>
        <source>Adicionar mapa de satelite</source>
        <translation type="obsolete">Add satellite map</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="193"/>
        <source>Criar mapa de rota do trator</source>
        <translation type="obsolete">Create tractor route map</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="200"/>
        <source>Criar mapa de velocidades do trator</source>
        <translation type="obsolete">Create tractor speed map</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="207"/>
        <source>Criar mapa de alarmes do Visum</source>
        <translation type="obsolete">Create Visum alarm map</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="227"/>
        <source>&amp;Mapa de plantio</source>
        <translation type="obsolete">&amp;Plantation map</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="226"/>
        <source>&amp;J.Assy - Mapa de plantio</source>
        <translation type="obsolete">&amp;J.Assy - Map of planting</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="289"/>
        <source>Projeto salvo com sucesso</source>
        <translation type="obsolete">Project succesfully saved</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="291"/>
        <source>Erro ao salvar projeto</source>
        <translation type="obsolete">Error saving project</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="296"/>
        <source>Info</source>
        <translation type="obsolete">Info</translation>
    </message>
    <message>
        <location filename="../visum_plantation_map.py" line="299"/>
        <source>Erro</source>
        <translation type="obsolete">Error</translation>
    </message>
</context>
</TS>
