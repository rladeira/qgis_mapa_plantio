FORMS = ../file_import_dialog.ui \
	../tractor_map_dialog.ui \
	../speed_map_dialog.ui \
	../alarm_map_dialog.ui

SOURCES = ../visum_map_of_planting.py \
	../file_import_dialog.py \
	../tractor_map_dialog.py \
	../speed_map_dialog.py \
	../alarm_map_dialog.py
	

TRANSLATIONS = VisumMapOfPlanting_pt.ts