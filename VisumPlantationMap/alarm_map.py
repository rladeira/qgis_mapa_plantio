# -*- coding: latin-1 -*-
from qgis.core import *
from PyQt4.Qt import QColor
from pm_utils import build_request, natural_sort

#TODO: Verificar de fazer s� at� o alarme 5
class AlarmMapManager:
    """ Rotinas para criar mapa de alarmes """
    
    NUM_LINES = 2
    
    #set de cores para utilizar no mapa de alarmes
    #COLOR_SET = [ (240,163,255),(0,117,220),(153,63,0),(76,0,92),(25,25,25),(0,92,49),(43,206,72),
    #         (255,204,153),(128,128,128),(148,255,181),(143,124,0),(157,204,0),(194,0,136),
    #         (0,51,128),(255,164,5),(255,168,187),(66,102,0),(255,0,16),(94,241,242),(0,153,143),
    #         (224,255,102),(116,10,255),(153,0,0),(255,255,128),(255,255,0),(255,80,5)]
    COLOR_SET = [ (255,2,157),(104,61,59),(255,116,163),
                 (150,138,232),(152,255,82),(167,87,64),(1,255,254),(255,238,232),(254,137,0),(189,198,255),
                 (1,208,255),(187,136,0),(117,68,177),(165,255,210),(255,166,254),(119,77,0),(122,71,130),
                 (38,52,0),(0,71,84),(67,0,44),(181,0,255),(255,177,103),(255,219,102),(144,251,146),
                 (126,45,210),(189,211,147),(229,111,254),(222,255,116),(0,255,120),(0,155,255),(0,100,1),
                 (0,118,255),(133,169,0),(0,185,23),(120,130,49),(0,255,198),(255,110,65),(213,255,0) ]
    
    COLOR_DICT = {  'FALHA NA ROTA��O DO DISCO': (1,0,103),
                    'FALHA DE SEMENTE': (232,94,190),
                    'FALHA DE ADUBO': (255,0,86),
                    'ENTUPIMENTO DE SEMENTE': (158,0,142),
                    'ENTUP. DE SEMENTE': (158,0,142),
                    'SENSOR DE ADUBO AUSENTE': (14,76,161),
                    'SEMENTE EMPERRADA DISCO': (255,229,2),
                    'VISOR ABERTO': (0,95,57),
                    'FALHA DE SEMENTE': (0,255,0),
                    'SENSOR DE SEMENTE AUSENTE': (149,0,58),
                    'ROTA��O MUITO ALTA NO DISCO': (255,147,126),
                    'SENSOR DE SEMENTE AUSENTE': (164,36,0),
                    'FALHA EM UMA SE��O': (0,21,68),
                    'ROTA��O DIFERENTE ENTRE SE��ES': (145,208,203),
                    'FALHA NO PLANTIO': (98,14,0),
                    'VELOCIDADE M�XIMA ULTRAPASSADA': (107,104,130),
                    'SEMENTES POR METRO ELEVADO': (0,0,255),
                    'SEMENTES POR METRO REDUZIDO': (0,125,181),
                    'TODAS AS LINHAS OK': (106,130,108),
                    'MANOBRA': (0,174,126),
                    'LINHA OK': (194,140,159),
                    'LINHA DESATIVADA': (190,153,112),
                    'LENTE SUJA': (0,143,156),
                    'MAR DE SEMENTE': (95,173,78),
                    'FALHA DE SEMENTE': (255,0,0),
                    'VELOCIDADE M�XIMA DE ROTA��O': (255,0,246) }

    def __init__(self, iface=None):
        self.iface = iface
        self.color_index = 0
        
    def get_color(self, categorie):
        color_tuple = None
        if categorie in self.COLOR_DICT: #If our categorie is enlisted we get it in the dictonary
            color_tuple = self.COLOR_DICT[categorie]
        else:
            color_tuple = self.COLOR_SET[self.color_index] #else we generate a random color
            self.color_index += 1
            if self.color_index == 26:
                self.color_index = 0
                
        return QColor(color_tuple[0], color_tuple[1], color_tuple[2])

    def create_map(self, layer, new_layer_name):
                
        new_layer = self.duplicate_layer(layer, new_layer_name)
        renderer = self.generate_render(new_layer)

        new_layer.setRendererV2(renderer)
 
        QgsMapLayerRegistry.instance().addMapLayer(new_layer)
        
    def duplicate_layer(self, layer, new_layer_name):
        new_layer = QgsVectorLayer(layer.source(), new_layer_name, layer.providerType())
        return new_layer
    
    def generate_categories(self, field, features):
        categories = []
        for feature in features:
            for i in range(0,self.NUM_LINES):
                if i == 0:
                    categorie = str(feature.attribute(field))
                else:
                    categorie = str(feature.attribute(field + "_" + str(i)))
                if not categorie in categories and categorie != None and categorie != 'NULL':
                    categories.append(categorie)
        
        return natural_sort(categories)
            
    def generate_rules(self, field, request, layer, label=''):
        categories = self.generate_categories(field, layer.getFeatures(request))
        rules = []
        for categorie in categories:
            expression = ""
            for i in range(0, self.NUM_LINES):
                if i == 0:
                    expression += '"' + field + '"'
                else:
                    expression += '"' + field + '_' + str(i) + '"'
                expression += '=\'' + categorie + '\''
                if i != self.NUM_LINES - 1:
                    expression += ' OR '
                    
            rules.append( (label + categorie, expression) )
        
        return rules
            
    #baseado em: https://snorfalorpagus.net/blog/2014/03/04/symbology-of-vector-layers-in-qgis-python-plugins/
    def generate_render(self, layer):
        request = build_request('ALARME', 'IS NOT NULL', 'OR', self.NUM_LINES)                 

        rules = self.generate_rules('ALARME', request, layer)
        symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())
        renderer = QgsRuleBasedRendererV2(symbol)
        
        root_rule = renderer.rootRule()
        
        for label, expression in rules:
            rule = self.set_rule(label, expression, root_rule, root_rule)
            
            request = QgsFeatureRequest().setFilterExpression(expression)
            sub_rules = self.generate_rules('LINHA', request, layer,label='LINHA ')

            color = self.get_color(label)
            for sub_label, sub_expression in sub_rules:
                self.set_rule(sub_label, sub_expression, rule, root_rule, color)
            
        root_rule.removeChildAt(0)
        
        return renderer
    
    def set_rule(self, label, expression, parent, root, color=None):
        rule = root.children()[0].clone()
        rule.setLabel(label)
        rule.setFilterExpression(expression)
        if color is None:
            rule.setSymbol(None)
        else:
            rule.symbol().setColor(color)
                               
        parent.appendChild(rule)
        
        return rule
            
