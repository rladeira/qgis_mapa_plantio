# -*- coding: latin-1 -*-

import os

from PyQt4 import QtGui, uic
from PyQt4.QtGui import QMessageBox
from pm_utils import populate_layerBox
from tractor_map import TractorMapManager

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'tractor_map_dialog.ui'))


class TractorMapDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(TractorMapDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)

        self.iface = None
        self.setupUi(self)
        self.txtLayerName.clear()
        self.btOk.clicked.connect(self.proccess_task)
        self.btCancel.clicked.connect(lambda: self.reject())
        self.cmbVLayer.currentIndexChanged.connect(self.update_layer_name)

    def initGui(self, iface):
        self.iface = iface
        self.txtLayerName.clear()
        populate_layerBox(self.cmbVLayer)

    def update_layer_name(self):
        if(self.cmbVLayer.currentIndex() >= 0):
            layer_name = self.cmbVLayer.itemData(self.cmbVLayer.currentIndex()).name()
            self.txtLayerName.setText(layer_name + "_rota")

    def proccess_task(self):
        if(self.cmbVLayer.currentIndex() < 0):
            self.accept()
            return
        
        if self.txtLayerName.text().isspace() or not self.txtLayerName.text():
            QMessageBox.critical(self, self.tr(u'Error'), self.tr(u'Layer name is mandatory'))
            return

        layer = self.cmbVLayer.itemData(self.cmbVLayer.currentIndex())
        manager = TractorMapManager(self.iface)
        manager.create_map(layer, self.txtLayerName.text())
        self.accept()