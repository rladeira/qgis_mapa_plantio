# -*- coding: latin-1 -*-
from qgis.core import *
from PyQt4.QtGui import *

class TractorMapManager:

    def __init__(self, iface=None):
        self.iface = iface

    def create_map(self, layer, new_layer_name):
        new_layer = QgsVectorLayer("LineString?crs=EPSG:4326", new_layer_name, "memory")
        if not layer.isValid():
            print "Layer failed to load!"
            return

        provider = new_layer.dataProvider()

        features = layer.getFeatures()
        #att_index = layer.dataProvider().fieldNameIndex('DATA REGISTRO') #Our points are already sorted
        points = []
        for feature in features:
            point = feature.geometry().asPoint()
            points.append(point)
        route = QgsGeometry.fromPolyline(points)
        
        feature = QgsFeature()
        feature.setGeometry(route)
        provider.addFeatures([feature])
        new_layer.updateExtents()
        
        symbol = QgsLineSymbolV2.defaultSymbol(new_layer.geometryType())
        symbol.setColor(QColor('black'))
        renderer = QgsSingleSymbolRendererV2(symbol)
        
        new_layer.setRendererV2(renderer)

        QgsMapLayerRegistry.instance().addMapLayer(new_layer)