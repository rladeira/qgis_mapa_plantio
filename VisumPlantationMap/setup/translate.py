# -*- coding: latin-1 -*-

import os
import sys
from subprocess import Popen, PIPE

try:
    os.chdir('../i18n')
    process = Popen(['pylupdate4', 'visum_map_of_planting.pro'], stdout=PIPE)
    process = Popen(['linguist', 'VisumMapOfPlanting_pt.ts'], stdout=PIPE)
except Exception as e:
    print (u'Falha na execu��o do script de tradu��o!\n')
    sys.exit(1)