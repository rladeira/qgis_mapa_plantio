# -*- coding: latin-1 -*-
"""
/***************************************************************************
 VisumMapOfPlanting
                                 A QGIS plugin
 Plugin para gerar de forma f�cil mapas de planta��es agr�colas com dados obtidos do sistema Visum da J. Assy
 Plugin used to ease the generation of agricultural maps with data obtained from J. Assy's Visum system
                              -------------------
        begin                : 2016-07-11
        git sha              : $Format:%H$
        copyright            : (C) 2016 by J. Assy Apollo Agr�cola
        email                : txai.garcia@assy.com.br
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon
from qgis.core import *
import qgis.utils
from qgis.gui import QgsMessageBar
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from file_import_dialog import FileImportDialog
from tractor_map_dialog import TractorMapDialog
from speed_map_dialog import SpeedMapDialog
from alarm_map_dialog import AlarmMapDialog
from project import ProjectManager

import os.path


class VisumMapOfPlanting:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'VisumMapOfPlanting_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlgs = {}
        self.dlgs['FileImport'] = FileImportDialog()
        self.dlgs['TractorMap'] = TractorMapDialog()
        self.dlgs['SpeedMap'] = SpeedMapDialog()
        self.dlgs['AlarmMap'] = AlarmMapDialog()
        self.project_manager = ProjectManager()
        #self.dlg = FileImportDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&J.Assy - Map of planting')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'VisumMapOfPlanting')
        self.toolbar.setObjectName(u'VisumMapOfPlanting')
        
        self.spin_lock = 0
        
        QgsProject.instance().projectSaved.connect(self.save_project_run)
        
    def tr(self, message):
        """Add the message to translation list.
    
        :param message: String to translate.
        :type message: str, QString
    
        :returns: Translated version of the String.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('VisumMapOfPlanting', message)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/visum_map_of_planting/icon.png'
                
        self.add_action(
            icon_path,
            text=self.tr(u'Import file from Visum'),
            callback=self.file_import_run,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)
        
        self.add_action(
            icon_path,
            text=self.tr(u'Add satellite map'),
            callback=self.open_google_maps_run,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)

        self.add_action(
            icon_path,
            text=self.tr(u'Add tractor route map'),
            callback=self.tractor_map_run,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)
        
        self.add_action(
            icon_path,
            text=self.tr(u'Add tractor speed map'),
            callback=self.speed_map_run,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)
        
        self.add_action(
            icon_path,
            text=self.tr(u'Add Visum alarm map'),
            callback=self.alarm_map_run,
            parent=self.iface.mainWindow(),
            add_to_toolbar=False)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&J.Assy - Map of planting'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar
        
        try:
            QgsProject.instance().savedProject.disconnect()
        except:
            pass

    def run(self, dlg):
        """Run method that performs all the real work"""
        #init gui
        dlg.initGui(self.iface)
        # show the dialog
        dlg.show()
        # Run the dialog event loop
        dlg.exec_()
        # See if OK was pressed
    
    #adaptado de http://gis.stackexchange.com/questions/120147/add-openlayers-layer-from-qgiss-python-console/120362#120362    
    def open_google_maps_run(self):
        mapProvider = 'Google' 
        QMSMap = 'Google Satellite' 

        webMenu = qgis.utils.iface.webMenu() 
        QMSMenu = None

        for webMenuItem in webMenu.actions():
            if 'QuickMapServices' in webMenuItem.text():               
                QMSMenu = webMenuItem      
        
        if QMSMenu is None:
            self.show_error_message(self.tr(u'QuickMapServices is not installed. Please install the plugin and try again'))
            return
            
        mapProviderMenu = None  
        
        for QMSMenuMenuItem in QMSMenu.menu().actions(): 
            if mapProvider in QMSMenuMenuItem.text():
                mapProviderMenu = QMSMenuMenuItem 
                
        if mapProviderMenu is None:
            self.show_error_message(self.tr(u'QuickMapServices is not configured. Please, configure the plugin and try again'))
            return
        
        for gmap in mapProviderMenu.menu().actions(): 
            if QMSMap in gmap.text(): 
                gmap.trigger() 
                                


    def file_import_run(self):
        """Opens the CSV import menu"""
        self.run(self.dlgs['FileImport'])
    
    def tractor_map_run(self):
        """Opens the tractor route map menu"""
        self.run(self.dlgs['TractorMap'])
        
    def speed_map_run(self):
        """Opens the tractor speed map menu"""
        self.run(self.dlgs['SpeedMap'])
        
    def alarm_map_run(self):
        """Opens the Visum alarm map menu"""
        self.run(self.dlgs['AlarmMap'])

    def save_project_run(self):
        """ Salva o projeto """
        if self.spin_lock == 0:
            self.spin_lock = 1
            try:
                self.project_manager.save_project()
                self.show_info_message(self.tr(u'Project saved successfully'))
            except:
                self.show_error_message(self.tr(u'Error saving the project'))
        else:
            self.spin_lock = 0
            
    def show_info_message(self, message):
        self.iface.messageBar().pushMessage(self.tr(u'Info'), message, level=QgsMessageBar.INFO, duration=3)
    
    def show_error_message(self, message):
        self.iface.messageBar().pushMessage(self.tr(u'Error'), message, level=QgsMessageBar.CRITICAL, duration=3)
