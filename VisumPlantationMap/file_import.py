# -*- coding: latin-1 -*-
import qgis.utils
from qgis.core import *
from PyQt4.QtGui import *

class FileImportManager:
    """ Rotinas de importação de arquivo CSV e parse """

    def __init__(self, iface=None):
        self.iface = iface

    def create_layer_from_file(self, filename, layername):
        #EPSG:4326
        uri = "file:///%s?encoding=latin1&crs=EPSG:4326&delimiter=%s&xField=%s&yField=%s" % (filename,",", "LONGITUDE", "LATITUDE")
        vlayer =  QgsVectorLayer(uri, layername, "delimitedtext")
        if not vlayer.isValid():
            print "Layer failed to load: ", layername # returns empty string
                  
        symbol = QgsMarkerSymbolV2.defaultSymbol(vlayer.geometryType())
        symbol.setColor(QColor('green'))
        renderer = QgsSingleSymbolRendererV2(symbol)
        vlayer.setRendererV2(renderer)
         
        QgsMapLayerRegistry.instance().addMapLayer(vlayer)    
                 
        self.iface.setActiveLayer(vlayer)
        self.iface.zoomToActiveLayer()
