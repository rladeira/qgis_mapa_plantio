@echo off
SET OSGEO4W_ROOT=C:\OSGeo4W64
call "%OSGEO4W_ROOT%"\bin\o4w_env.bat
call "%OSGEO4W_ROOT%"\apps\grass\grass-6.4.3\etc\env.bat
@echo off
path %PATH%;%OSGEO4W_ROOT%\apps\qgis\bin
path %PATH%;%OSGEO4W_ROOT%\apps\grass\grass-6.4.3\lib

set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\Python27
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\Python27\Lib
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\Python27\sip
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\Python27\Lib\site-packages
set PYTHONPATH=%PYTHONPATH%;C:\Users\txai.garcia\.p2\pool\plugins\org.python.pydev_5.1.2.201606231256\pysrc
set QGIS_PREFIX_PATH=%OSGEO4W_ROOT%\apps\qgis
set PYTHONHOME=%OSGEO4W_ROOT%\apps\Python27
set PATH=%OSGEO4W_ROOT%\apps\qgis; %OSGEO4W_ROOT%\bin;C:\Program Files\Git\cmd;C:\Program Files (x86)\GnuWin32\bin;C:\cygwin64\bin;C:\Program Files\7-Zip;%PATH%
cd %HOMEPATH%\development
start "Eclipse aware of Quantum GIS" /B "C:\Users\txai.garcia\eclipse\cpp-neon\eclipse\eclipse.exe" %*